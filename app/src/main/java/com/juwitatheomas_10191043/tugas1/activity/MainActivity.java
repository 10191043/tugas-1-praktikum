package com.juwitatheomas_10191043.tugas1.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.juwitatheomas_10191043.tugas1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}